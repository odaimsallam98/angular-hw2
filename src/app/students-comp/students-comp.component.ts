import { Component, OnInit } from '@angular/core';
import {Student} from './Student';

@Component({
  selector: 'app-students-comp',
  templateUrl: './students-comp.component.html',
  styleUrls: ['./students-comp.component.css']
})

export class StudentsCompComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }
  
  Students: Student[]=[
    {id:187070,name:"odai1",major:"major1",avg:91},
    {id:186070,name:"odai2",major:"major2",avg:89},
    {id:188080,name:"odai3",major:"major3",avg:60},
    {id:189090,name:"odai4",major:"major4",avg:45},
   ];

}
